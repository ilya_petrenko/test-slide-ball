﻿using UnityEngine;

namespace Balls.Trigger.Model
{
    public class TriggerModel : MonoBehaviour
    {
        [SerializeField]
        private TriggerType _triggerType;
        
        public TriggerType TriggerType
        {
            get { return _triggerType; }
        }
    }
}