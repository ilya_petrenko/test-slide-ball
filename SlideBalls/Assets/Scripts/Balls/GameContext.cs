using Balls.Ball.Service;
using UnityEngine;

namespace Balls
{
    public class GameContext : MonoBehaviour
    {
        private static GameContext _instance; 
    
        private void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
                return;
            }
            _instance = this;
            InitLocation();
        }

        private void InitLocation()
        {
            new BallService().Init();
        }
    
        public static GameContext Instance
        {
            get { return _instance; }
        }
    }
}
