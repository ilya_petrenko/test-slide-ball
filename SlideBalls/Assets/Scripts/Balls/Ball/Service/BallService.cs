﻿using Balls.Ball.Controller;
using Balls.Ball.Model;
using UnityEngine;

namespace Balls.Ball.Service
{ 
    public class BallService
    {
        public void Init()
        {
            BallModel[] ballModels = GetLocation().GetComponentsInChildren<BallModel>();
            foreach (BallModel b in ballModels) {
                InitBall(b);
            }
        }
        private void InitBall(BallModel ballModel)
        {
            BallController ballController = ballModel.gameObject.AddComponent<BallController>();
            BallBehavior ballBehavior = ballModel.gameObject.AddComponent<BallBehavior>();
            ballController.Init(ballModel);
            ballBehavior.Init(ballController);
        }
        private GameObject GetLocation()
        {
            return GameContext.Instance.gameObject;
        }
    }
}