﻿using UnityEngine;

namespace Balls.Ball.Controller
{
    public class BallBehavior : MonoBehaviour
    {
        private BallController _controller;
        private BallBehaviorType _behaviorType;
        
        public void Init(BallController controller)
        {
            _controller = controller;
            _controller.OnTriggerEndPosition += OnTriggerEndPosition;
            SetBallInitBehavior();
        }

        private void OnDestroy()
        {
            _controller.OnTriggerEndPosition -= OnTriggerEndPosition;
        }
        private void SetBallInitBehavior()
        {
            BehaviorType = BallBehaviorType.PHYSICS;
        }
        private void OnTriggerEndPosition()
        {
            BehaviorType = BallBehaviorType.AI;
        }
        private void Update()
        {
            if (BehaviorType == BallBehaviorType.PHYSICS) {
                return;
            }
            if (_controller.IsStartPosition()) {
                BehaviorType = BallBehaviorType.PHYSICS;
            }
        }

        private BallBehaviorType BehaviorType
        {
            get { return _behaviorType; }
            set
            {
                if (value == BallBehaviorType.AI) {
                    _controller.PhysicsEnabled = false;
                    _controller.NavMeshAgentEnabled = true;
                    _controller.Brighted = true;
                    _controller.StartMoving();
                    return;
                }
                _controller.Brighted = false;
                _controller.PhysicsEnabled = true;
                _controller.NavMeshAgentEnabled = false;
             
            }
        }
        
    }
    
    public enum BallBehaviorType
    {
        AI,
        PHYSICS
    }
}