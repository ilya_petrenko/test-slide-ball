﻿using System;
using Balls.Ball.Component;
using Balls.Ball.Model;
using Balls.Trigger.Model;
using UnityEngine;
using UnityEngine.AI;

namespace Balls.Ball.Controller
{
    public class BallController : MonoBehaviour
    {
        private Transform _startPosition;
        private Rigidbody _rigidbody;
        private NavMeshAgent _agent;
        private MaterialBrightedComponent _materialBrightedComponent;
        
        public event Action OnTriggerEndPosition;
        public void Init(BallModel ballModel)
        {
            _startPosition = ballModel.StartPosition;
            _rigidbody = GetComponentInChildren<Rigidbody>();
            _agent = GetComponentInChildren<NavMeshAgent>();
            _materialBrightedComponent = GetComponentInChildren<MaterialBrightedComponent>();

        }
        private void OnTriggerEnter(Collider collider)
        {
            OnTrigger(collider);
        }

        public bool IsStartPosition()
        {
            Vector3 position = _startPosition.position;
            Vector3 position1 = transform.position;
            return Math.Abs(position.x - position1.x) < 0.3f
                   && Math.Abs(position.y - position1.y) < 0.3f;
        }
        public void StartMoving()
        {
            _agent.destination = _startPosition.position;
        }
        private void OnTrigger(Collider collider)
        {
            TriggerModel triggerModel = collider.GetComponent<TriggerModel>();
            if (triggerModel == null) {
                return;
            }
            if (triggerModel.TriggerType != TriggerType.END_BALL_PLACE) {
                return;
            }
            OnTriggerEndPosition?.Invoke();
        }
        
        public bool PhysicsEnabled
        {
            get { return !_rigidbody.isKinematic; }
            set { _rigidbody.isKinematic = !value; }
        }
        public bool NavMeshAgentEnabled
        {
            get { return _agent.enabled; }
            set { _agent.enabled = value; }
        }
        public bool Brighted
        {
            get { return _materialBrightedComponent.Brighted; }
            set { _materialBrightedComponent.Brighted = value; }
        }
    }
}