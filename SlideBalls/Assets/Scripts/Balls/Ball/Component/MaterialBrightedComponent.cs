﻿using UnityEngine;

namespace Balls.Ball.Component
{
    public class MaterialBrightedComponent : MonoBehaviour
    {
        [SerializeField]
        private Material _normalMaterial;
        [SerializeField]
        private Material _brightMaterial;

        private Renderer _renderer;

        private bool _brighted;

        private void Awake()
        {
            _renderer = GetComponent<Renderer>();
        }

        public bool Brighted
        {
            get { return _brighted; }
            set
            {
                if (!enabled) {
                    return;
                }
                if (_brighted == value) {
                    return;
                }
                _brighted = value;
                _renderer.material = value ? _brightMaterial : _normalMaterial;
            }
        }
    }
}