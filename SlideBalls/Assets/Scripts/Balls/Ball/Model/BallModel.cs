﻿using UnityEngine;

namespace Balls.Ball.Model
{
    public class BallModel : MonoBehaviour
    {
        [SerializeField]
        private Transform _startPosition;

        public Transform StartPosition
        {
            get { return _startPosition; }
        }
    }
}